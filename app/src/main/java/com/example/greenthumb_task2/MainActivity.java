package com.example.greenthumb_task2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<itemlist> listModule;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listModule = new ArrayList<>();
        listModule.add(new itemlist("veg","discription","prize",R.drawable.veg));
        listModule.add(new itemlist("veg1","discription","prize",R.drawable.veg1));
        listModule.add(new itemlist("veg2","discription","prize",R.drawable.veg2));
        listModule.add(new itemlist("veg3","discription","prize",R.drawable.veg3));
        listModule.add(new itemlist("veg4","discription","prize",R.drawable.veg4));
        listModule.add(new itemlist("veg5","discription","prize",R.drawable.veg5));
        listModule.add(new itemlist("veg6","discription","prize",R.drawable.veg6));


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, listModule);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);
    }
}