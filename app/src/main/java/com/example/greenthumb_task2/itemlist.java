package com.example.greenthumb_task2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class itemlist extends AppCompatActivity {
    private String Title;
    private String Description;
    private int Image;
    private  String Price;

    public itemlist(String veg, String discription, String price, int i) {
        Title = veg;
        Description = discription;
        Image = i;
        Price = price;
    }

    @NonNull
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getTitle() {
      return Title;
    }

    public int getImage() {
        return 0;
    }



    public String getDescription() {
        return Description;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setImage(int image) {
        Image = image;
    }
    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}